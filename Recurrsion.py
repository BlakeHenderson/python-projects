#Blake Henderson and Sebastain Schiller
#Gregory Bickerman
#Computer Programming - D
#January 16, 2014

def isPalindrome(string):
    """Returns True if a string is a Palindrome or False if it is not"""
    #Base Cases
    if string == "":
        return True

    if string[0] != string[-1]:
        return False

    if string[0] == string[-1]:
        rest = string[1:-1]
        Palindrome = isPalindrome(rest)
        return Palindrome
        
def pow(x,y):
    """Returns X^Y"""
    #Base Cases
    if y == 0:
        return 1

    #Recursion Step
    elif y < 0:
        return -x*pow(x,y-1)
    else:
        return x*pow(x,y-1)

def containsA(string):
    """Returns True if sting contains an A, False Otherwise"""
    #Base Case
    if string == "":
        return False

    if string[0] == "a":
        return True

    if string[0].lower() != "a":
    #Main Step
        RestOfString = string[1:]
        return containsA(RestOfString)
    
def countA(string):
    """Returns the number of A's in string"""

    rest = string [1:]
            
    if string == "":
        return 0

    if string[0] == "a":
        return countA(rest)+1

    if string[0].lower() != "a":
        numA = countA(rest)
        return numA


def IndexOfA(string):
    #Base Case
    if string == "":
        return -1

    if string[0].lower() == "a":
        return 0

    if string[0].lower() != "a":
    #Recursion
        rest = string[1:]
        pos = IndexOfA(rest)
        if pos == -1:
            return -1
        else:
            return pos + 1

def collatz(n):
    """Finds how many times it takes to get back to the number after checking
    Multiple times to see if its even or odd and doing
    Cooresponding Operations"""
    accumNum = 0
    
    #Base Case
    if n == 1:
        return 0

    #Recursive Step
    else:
        return collatz(HOTPO(n))+1

def HOTPO(n):
    """Checks if its Even or Odd and gives back the correspponding number"""
    if n%2 == 0:
        return n/2

    else:
        return n*3+1
