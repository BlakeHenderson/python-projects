class TicTacToe:

    def __init__(self):
        self.board = [[" "," "," "],
                      [" "," "," "],
                      [" "," "," "]]

        self.xLog = []
        self.oLog = []

        self.currPlayer = "X"

    def __repr__(self):
        rep = ""
        for row in self.board:
            for place in row:
                rep += "|" + place
            rep += "|\n"
        rep = rep[:-1]
        return rep
        
    def makeMove(self, row, col):
        """Current player makes a move at the given row and col"""
        self.board[row][col] = self.currPlayer
        if self.currPlayer == "X":
            self.xLog.append((row,col))
        else:
            self.oLog.append((row,col))

    def reset(self):
        """Resets the playing board"""
        for row in self.board:
            for elemInt in range(len(row)):
                row[elemInt] = " "
        self.xLog = []
        self.oLog = []

    def swapPlayer(self):
        """Makes the other player the current player"""
        if self.currPlayer == "X":
            self.currPlayer = "O"
        else:
            self.currPlayer = "X"

    def hasWon(self):
        """Returns True if the current player has won;
           False otherwise
        """
        magic = [[6,7,2],[1,5,9],[8,3,4]]
        if self.currPlayer == "X":
            log = self.xLog
        else:
            log = self.oLog

        for p1 in range(len(log)):
            for p2 in range(p1+1, len(log)):
                    for p3 in range(p2+1, len(log)):
                            (r1,c1) = log[p1]
                            (r2,c2) = log[p2]
                            (r3,c3) = log[p3]
                            if magic[r1][c1] + magic[r2][c2] + magic[r3][c3] == 15:
                                return True
                            
        return False
    
    def isDraw(self):
        """Returns True if the game is a draw;
           False otherwise
        """
        for row in self.board:
            for element in row:
                if element == " ":
                    return False
        return True

    def isValid(self, row, col):
        """Checks to see if a players move is valid"""
        if row > 2 or col > 2:
            return False
        if self.board[row][col] != " ":
            return False
        else:
            return True
                    

    def play(self):
        """Plays a game of TicTacToe"""

        #Declares constant variables
        validInput = False
        gameOver = False
        
        print "\n Welcome to Tic Tac Toe! (Created by Blake Henderson and Sebastian Schiller) \n"
        print "Main Menu:"
        print "(1) Play"
        print "(2) Exit"
        print ""

        while validInput == False:
            userInput = raw_input("Choose a Option: ")
            if userInput == "1":
                while gameOver == False:
                    #Prints the current board
                    print "\n" + self.__repr__()
                    
                    #Asks the Player to make a Move
                    playerMoveRow = int(raw_input("\nPlayer " + str(self.currPlayer) + ", Insert a Row Number (0-2): "))
                    playerMoveColumn = int(raw_input("Player " + str(self.currPlayer) + ", Insert a Column Number (0-2): "))


                    #If the move is valid
                    if self.isValid(playerMoveRow, playerMoveColumn) == False:
                        print "Invalid - Please try another number"
                    else:
                        #Moves the player to the desired position
                        self.makeMove(playerMoveRow, playerMoveColumn)

                        #Checks to see if it is a draw
                        if self.isDraw() == True:
                            print "You Tied!"
                            gameOver = True
                            
                        #Checks to see if the player has won
                        if self.hasWon() == True:
                            print "Congradulations! Player " + str(self.currPlayer) + " has Won the Game!"
                            gameOver = True
                        
                        #If the player did not win, the player is swapped and the game is continued
                        self.swapPlayer()
    
                gameOver = False
                self.reset()
            if userInput == "2":
                exit()

            else:
                print "Please input either 1 or 2"
                    

        

        
