#Blake Henderson
#Stock Market Simulator
import random

class Stock:
    def __init__(self, name, symbol, price, volatility, average):
        self.name = name
        self.symbol = symbol
        self.price = price
        self.volatility = volatility
        self.average = average
        self.currEvent = ""

    def __repr__(self):
        return  "NAME: " + self.name + "\n" + "PRICE: " + str(self.price) +"\n"
                                                                                                                                                   
        
        
    def tick(self):
        self.price += random.gauss(self.average ,self.volatility)

    def changeStock(self, event):
        self.currEvent = event.text
        self.volatility += event.vChange
        self.average += event.aChange
    
class Event:
    def __init__(self, text, vChange, aChange):
        self.text = text
        self.vChange = vChange
        self.aChange = aChange

    def sendEvent(self):
        return [self.text, self.vChange, self.aChange]


#Create a Stock
sm = Stock("Apple", "AAPL", random.uniform(1, 1000), random.uniform(0, 5), random.uniform(-1, 1))
print str(sm)

#Simulates Ticking
for x in range(10):
    sm.tick()
    print sm
    
#Chooses a random event
eventsList = [Event("Expectations are high!", 1, .5),
    Event("New Products Expected, Customers seem excited!", 1, .7),
    Event("Expectations are Low, poor customer satisfaction", 1, -.5),
    Event("New Product was a failure, causing mass amounts of money loss", 1, -.8),
    Event("New Healthcare plan expected!", 1, .9)]

randomEvent = random.choice(eventsList)

#The Event Changes the Stocks Volatility and Average, Affecting Price Over Time
sm.changeStock(randomEvent)

print "CHANGED VOLATILITY AND AVERAGE" + "\n" + randomEvent.text + "\n"

#Prints final Answer at the End
for x in range(10):
    sm.tick()
    print sm
    
