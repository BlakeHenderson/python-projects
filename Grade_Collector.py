def mainMenu():
    """Main Menu Function"""

    #Print the Menu
    print "\n The Benjamin School Grader | Copyright 2013 \n"
    print "Welcome! Please choose one of the following options"
    print "[1]: Add Grades"
    print "[2]: Average - (Only if there are more than 3 grades inputed)"
    print "[3]: See all grades"
    print "[4]: Quit"

    mainGradeList = []

    #Infinite Loop
    while True:
        #Ask for Raw_Input
        userMenuInput = raw_input("Choose a Option: ")

        #Check if it is a digit
        if userMenuInput.isdigit() and int(userMenuInput) <= 4:
            #Check the User Input to send to the proper function
            if userMenuInput == "1":
                addGrades(mainGradeList)
            elif userMenuInput == "2":
                findAverage(mainGradeList)
            elif userMenuInput == "3":
                allGrades(mainGradeList)
            elif userMenuInput == "4":
                #Quit Python Script
                quit(mainGradeList)
        else:
            #If the Input is not 1 - 4 it will say invalid input
            print "Invalid Input"
        
        
def addGrades(mainGradeList):
    """Collects grades from the user and adds them to a list"""
    
    print "To Quit: Type (End) \n"
    
    while True:
        #Asks for a user Input
        userGradeInput = raw_input("Input a Grade (Numbers Only): ")

        #Checker
        if userGradeInput.lower() == "end":
            return
            
        else:
            #Adds the Number to the list in the main function
            if userGradeInput.isdigit():
                mainGradeList.append(userGradeInput)
            else:
                print "Invaid Input (Positive Numbers Only)"

def findAverage(mainGradeList):
    """ Takes mainGradeList and find the average of the numbers, however there must
        be more than three numbers."""
    #Accumulator variable
    total = 0

    #Checks if the mainGradeList is greater than 3
    if len(mainGradeList) >= 3:

        #Extractor
        for x in range(len(mainGradeList)):
            extract = mainGradeList[x]
            total += int(extract)
            
         #Finds and Prints Average                          
        final = total/len(mainGradeList)
        print "The Average is: " + str(final)
        
    else:
        #Prints a Error Message
        print "You Need to Input more than 3 Grades!"
        return

def allGrades(mainGradeList):
    """Prints out all of the inputed grades"""

    print "\n All Grades\n"

    #Extracts all grades and prints them
    if len(mainGradeList) >= 1:
        for x in range(len(mainGradeList)):
            print mainGradeList[x]
        print ""
    #If no Grades, Prints "No Grades!"
    else:
        print "\n No Grades! \n"
        return 
                

#Runs Main Function 
mainMenu()
