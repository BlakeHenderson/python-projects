#Blake Henderson and Sebastain Schiller 
#Mr. Bickerman
#Computer Programming - D
#December 10th, 2013

import random

def main():
    """Runs Main Function"""

    print "Hangman 2013 | Blake Henderson and Sebastain Schiller"
    print ""
    print "Welcome to Hangman! 2.0"

    print ""
    print "Main Menu"
    print "(1) Play Game"
    print "(2) Leaderboard"
    print "(3) Quit Game"

    leaderboardList = []
    
    while True:
        #Ask for Raw_Input
        userMenuInput = raw_input("Choose a Option: ")

        #Check if it is a digit
        if userMenuInput.isdigit() and int(userMenuInput) <= 3:
            #Check the User Input to send to the proper function
            if userMenuInput == "1":
                difficultyLevel = raw_input("What Difficulty would you like? (1,2,3)(Easy,Medium,Hard): ")
                if difficultyLevel.isdigit() == False:
                    print "Invalid Input, Please enter a number between 1 - 3"
                else:
                    if int(difficultyLevel) >= 4:
                        print "Invalid Input, Please enter a number between 1 - 3"
                    else:
                    #Based on UserInput changes the Difficulty
                        if difficultyLevel == "1":
                            HangMan(15)
                        elif difficultyLevel == "2":
                            HangMan(10)
                        elif difficultyLevel == "3":
                            HangMan(5)
            elif userMenuInput == "2":
                leaderboardPrint()
            elif userMenuInput == "3":
                quit(main)
        else:
            #If the Input is not 1 - 3 it will say invalid input
            print "Invalid Input, Please enter a number between 1 - 3"

def openFile():
    """Opens a File"""
    wordList = open("words.txt","r")
    words = wordList.read().split()
    randWord = random.choice(words).upper()

    return randWord

def HangMan(Difficulty):
    """Plays the game Hangman"""
    lettersGuessed = []
    guessCounter = 0
    endGame = True
    numGuessed = 0
    numWrong = 0
    randWord = openFile()

    print "\n The Computer has chosen a word! \n "

    while endGame == True:
        userLetterInput = raw_input("Guess a Letter?: ").upper()
        #Check if the userLetterInput is a Letter and is greater than 1
        if len(userLetterInput) > 1 or userLetterInput.isalpha() == False: 
            print "Invalid Input, Please input a Letter"
        else:
            #Check if the letter has already been guessed
            if userLetterInput in lettersGuessed:
                print "You've Already Guessed that letter! Try Again"
                        
            else:
                lettersGuessed.append(userLetterInput)
                print  displayWord(randWord,lettersGuessed)
                numGuessed += 1
                
                if not userLetterInput in randWord: #Is the User is wrong 
                    print str(userLetterInput) + " Is not in the word!"
                    print "Try Again!"
                    print "You Have: " + str(Difficulty - numWrong) + " Lives left!"
                    numWrong += 1
                
                if numWrong > Difficulty: #If the user has Lost
                    print "The Hangman has Died! :("
                    print "You were wrong " +str(numWrong) + " Times!"
                    print "The Word Was: " + str(randWord)
                    return
                    
                if checkWord(randWord, lettersGuessed) == True: #if the user has won the game
                    print ""
                    print "You've Guessed the Word Correctly!"
                    print "The Word was " + randWord
                    print "You Guessed " + str(numGuessed)+ " Times!"
                    print "You were wrong " +str(numWrong) + " Times!"
                    print " "
                    print " "
                    #Ask for Leaderboard Information
                    leaderboardUserInput = raw_input("Enter you Three Initials (End if you would not like to participate): ")
                    
                    if leaderboardUserInput.lower() == "end":
                        return
                    else:
                        if len(leaderboardUserInput) > 3 or userLetterInput.isalpha() == False:
                            print "Invalid Input. Try Again using only Three Letters!"
                        else:
                            scores = [leaderboardUserInput, str(numGuessed),randWord]
                            leaderboardWrite(scores)
                            print "Thank You for your information!"
                            return

def displayWord(word,lettersGuessed):
    """Takes a word and a list as a argument and prints out all of the letters in the
    word, leaving underscores in place of letters that have not been guessed yet.
    """
    finalString = ""

    for letter in word:
        if letter in lettersGuessed:
            finalString += " " + letter + " "
        else:
            finalString += " _ "

    return finalString

def checkWord(word,lettersGuessed):
    """Takes a word and a list of guessed letters and returns
    TRUE if all of the letters in the word have been gussed and FALSE otherwise.
    """
    
    for letter in word:
        if not letter in lettersGuessed:
            return False
    return True 

def leaderboardWrite(scores):
    """Opens a File containing the leaderboard information and displays it"""
    Leaderboard = open("leaderboard.lbd","w")
    #Writes to File the Previous and New Scores
    for line in scores:
        for word in line:
            Leaderboard.write(word + " ")
        Leaderboard.write("\n")
    Leaderboard.close()

def leaderboardRead(filename):
    scores = []
    leaderboard = open(filename,"r")
    for line in leaderboard:
        userInfo = line.split()
        scores.append(userInfo)
    leaderboard.close()
    return scores

def leaderboardPrint():
    scoreList = leaderboardRead("leaderboard.lbd")
    print "Initials:" + "     " + "Number of Times Guessed:" + "     " + "Word Guessed:"
    for line in scoreList:
       for word in line:
           print word,
       print " "

main()

